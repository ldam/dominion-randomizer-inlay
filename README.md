# Dominion Randomizer
This project aims to create an electronic randomization device to place inside a standard Dominion boardgame box, helping playing randomized games.

Typical when playing a randomized game one chooses 10 cards from the randomizer cards, and uses this selection to play with. This process takes some time and is a bit cumbersome.

This device aims to speed up the randomization process and overcome the manual process, so a faster game turnaround time can be achieved

## Appetizer pics and video

|Assembled|parts|Semi assembled|
|---------|-----|--------------|
|![Assembled unit](res/assembled_thm.jpg "Assembled unit")|![Disassembled unit](res/parts_thm.jpg "Disassembled unit")|![Semi assembled unit](res/top_and_bottom_thm.jpg "Semi assembled unit")|
|[Large](res/assembled.jpg)|[Large](res/parts.jpg)|[Large](res/top_and_bottom.jpg)|


<div class="video-fallback">
  See the video: <a href="https://www.youtube.com/watch?v=Xf21WcSykfg">Dominion randomizer inlay</a>.
</div>
<figure class="video-container">
  <iframe src="https://www.youtube.com/embed/Xf21WcSykfg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>



## User manual
This section describes the user facing operations (normal usage and configuration) of the Dominion randomizer.

### Features of the dominion randomizer
The dominion randomizer is placed in the inlay position in the game box.

Press the 'new game' button and the randomizer selects 10 kingdom cards for you and shows these with a green light next to the selected kingdom cards. These 10 cards should be the ones to play.

Continuing games will select new randomized cards.

To help clean up the old games, old cards that are not selected are shown with a red light, so they are easy to put back in the box.

Furthermore the Dominion Randomizer also helps select which player to start. 
A yellow light shows which player to start in a 3 player game, and a blue light shows which player to start in a 2/4 player game.

The treasure cards slots are used to select starter in a 3 player game.
Before a game session is started players decide which treasure card represents them, so e.g. copper would be player andy, silver would be player bob and gold would be player claire.
Whenever gold lights up in a new game, claire starts, etc.

Same mechanism for 2/4 players, but the blue lights selection is tied to the victory cards: Curse, Estate, Duchy, Province. In a two player game each player selects two victory cards that represents them.

### Usage instructions
The device has two buttons:

* Red: 'new game' (normal press), 'new session' (long press)
* Green: 'show game' (normal press), 'configuration' (long press)

#### New game (short press on red button)
Starts a new game and randomizes the selection and starting player'; the new setup is shown for ca 60 seconds.
Green cards go on the table, red cards goes from the table and back in the box.

#### New session (long press on red button)
Starts a new session and shows 10 green kingdom cards to play initially

#### Show game (short press on green button)
Shows the current game setup for 60 secs.

If one has not managed to setup the board in the 60 seconds given after a new game is initiated, pressing the 'show game' button shows the current setup for 60 seconds, so setup can be finalized.

When the selection is shown, one can turn it off by pressing either the green or red button.

#### Configuration (long press on the green button)
Entering the configuration mode allows you to change the mode of each slot (e.g. change the Randomized to other Dominion versions)

Slot types:

* Green - Kingdom cards
* Yellow - Player selection group 1 (typical used for 3 player games)
* Blue - Player selection group 2 (typical used for 2/4 player games)
* White - Slot not in use

One slot will flash as being edited, and pressing the red button will toggle between the slot types

Pressing the green button will move the current selection to the next slot

Long press again on the green button will cancel the configuration,
whereas long press on the red button will save the configuration.

If neither save or cancel has been pressed for one minute since last press, configuration mode will end without saving the configuration.

About player the selection groups: Each game start will randomize one selection in each the two player selection groups, regardless of how many slots is utilized by the group. 
E.g. configuring 0 slots of a group will disable the group. Configuring 5 slots of a group will let you use the Dominion Randomizer for 5 player games.
Typically one group is configured for 3 players, and the other for 2/4  players.

## Construction manual
In this project there are a number of sub folders:

* cad - contains mechanical 3d cad drawing of the box - can be used to either CNC or 3D print the box.
* sw - contains the micro controller software
* hw - contains electric diagram and pcb layout
* doc - contains paper overlay, paper instructions, GPLv3 license
* artifacts - contains final cad/sw/hw files so you will not have to build everything from scratch. e.g. HEX file for the micro controller, gcode/stl files for cnc/3d devices, gerber files for pcb production.

Each of these subfolders contain a README.md explaining in detail how to each of these elements are designed and how you can build them

### Assembly
To build this project, you would need to:

1. Build the pcb
2. Compile the program and download it to the microcontroller
3. Print/cut a box
4. Print a paper overlay (or on an overhead transparent film)
5. Glue/fix these parts together

Done.

### Power
The device contains a 220mah L-ion battery, when fully charged it should be good for 150+ games. There is a USB port, plugging in a USB charger will charge the battery.

## Tools
The project aims to use as much open source software as possible to create the Dominion Randomizer; 
in general the project has been developed on a linux based system, and the tool selection reflects this.

One thing though: to program the micro controller, a commercial hardware usb device, 'Microchip PICkit(tm) 2' was used during development to download the software.
You may find other options, however this supported via the pk2cmd command line tool on linux.

This is the list of tools primarily used to build the Dominion Randomizer:

* cad
  * Freecad 0.18 (daily) for designing the box
  * (LinuxCNC to cnc cut the box - was actually not used during development, but ought to be preferred. UCCNC was used in the development)
  * (at the time of writing 3D printing option was not explored, but there should be no reason this should not be possible - anyone?)
* sw
  * sdcc 3.9.0 with microchip non-free extension (project is based on a Microchip pic18f14k22 microcontroller)
  * pk2cmd to download HEX file to micro controller
  * make for make files
  * Whatever source code editor you prefer
* hw
  * Geda gschem to draw schematics
  * Geda refdes_renum
  * Geda gsch2pcb
  * Geda pcb create PCB design
  * gerbv to view pcb output and produce production files
  * pcb2gcode to create gerber file for laser stencil cutting
* doc
  * paper overlay
  * paper instructions
  * GPLv3 license

Some tools may have been omitted due to either being default available on a linux system or due to absentmindedness

## Suggestions/discussion/help
Open an issue in this project

## License
All parts of the dominion Randomizer project is released under the **GPL v3** Licence

