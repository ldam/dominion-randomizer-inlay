
Element["" "" "" "" 19.5000mm 68.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-1.5000mm -1.5000mm 1.5000mm -1.5000mm 2.0000mm 20.00mil 2.2540mm "" "1" "square"]
	Pad[-1.5000mm 1.5000mm 1.5000mm 1.5000mm 2.0000mm 20.00mil 2.2540mm "" "2" "square"]
	ElementLine [-1.2500mm -2.7500mm 2.7500mm -2.7500mm 6.00mil]
	ElementLine [2.7500mm -2.7500mm 2.7500mm 2.7500mm 6.00mil]
	ElementLine [2.7500mm 2.7500mm -1.5000mm 2.7500mm 6.00mil]

	)
