
Element["" "" "" "" 3.0770mm 4.4068mm 0.0000 0.0000 0 100 ""]
(
	Pin[0.9000mm 2.0000mm 26.00mil 12.00mil 30.02mil 24.02mil "" "10" "edge"]
	Pin[0.9000mm -2.0000mm 26.00mil 12.00mil 30.02mil 24.02mil "" "11" "edge"]
	Pad[-1.2500mm -3.5000mm -1.2500mm -3.1000mm 1.3000mm 20.00mil 1.5540mm "" "6" "square"]
	Pad[-1.2500mm 3.1000mm -1.2500mm 3.5000mm 1.3000mm 20.00mil 1.5540mm "" "7" "square,edge2"]
	Pad[1.1000mm 2.8500mm 1.1000mm 3.6000mm 0.9000mm 20.00mil 1.1540mm "" "8" "square,edge2"]
	Pad[1.1000mm -3.6000mm 1.1000mm -2.8500mm 0.9000mm 20.00mil 1.1540mm "" "9" "square"]
	Pad[0.8500mm 1.3000mm 2.3000mm 1.3000mm 0.4000mm 20.00mil 0.6540mm "5" "5" "square,edge2"]
	Pad[0.8500mm -1.3000mm 2.3000mm -1.3000mm 0.4000mm 20.00mil 0.6540mm "1" "1" "square,edge2"]
	Pad[0.8500mm -0.6500mm 2.3000mm -0.6500mm 0.4000mm 20.00mil 0.6540mm "2" "2" "square,edge2"]
	Pad[0.8500mm 0.0000 2.3000mm 0.0000 0.4000mm 20.00mil 0.6540mm "3" "3" "square,edge2"]
	Pad[0.8500mm 0.6500mm 2.3000mm 0.6500mm 0.4000mm 20.00mil 0.6540mm "4" "4" "square,edge2"]
	ElementLine [-2.4500mm -4.0000mm -2.4500mm 4.0000mm 6.00mil]

	)
