
Element["" "" "" "" 4.5000mm 14.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-2.8966mm 1.5912mm -2.1966mm 1.5912mm 1.0000mm 12.00mil 1.3048mm "" "2" "square"]
	Pad[2.2034mm -1.6088mm 2.9034mm -1.6088mm 1.0000mm 12.00mil 1.3048mm "" "4" "square,edge2"]
	Pad[-2.8966mm -1.6088mm -2.1966mm -1.6088mm 1.0000mm 12.00mil 1.3048mm "" "1" "square"]
	Pad[2.2034mm 1.5912mm 2.9034mm 1.5912mm 1.0000mm 12.00mil 1.3048mm "" "3" "square,edge2"]
	ElementLine [-2.4966mm -2.5088mm 2.5034mm -2.5088mm 6.00mil]
	ElementLine [-2.4966mm 2.4912mm 0.4714mm 2.4912mm 6.00mil]
	ElementLine [2.5034mm -2.5088mm 2.5034mm -2.2088mm 6.00mil]
	ElementLine [2.5034mm -1.0088mm 2.5034mm 0.4592mm 6.00mil]
	ElementLine [-2.4966mm -2.5088mm -2.4966mm -2.2088mm 6.00mil]
	ElementLine [-2.4966mm 2.4912mm -2.4966mm 2.1912mm 6.00mil]
	ElementLine [-2.4966mm -1.0088mm -2.4966mm 0.9912mm 6.00mil]
	ElementLine [2.5034mm 0.4592mm 2.5034mm 0.5392mm 6.00mil]
	ElementLine [2.5034mm 0.5392mm 2.1878mm 0.8548mm 6.00mil]
	ElementLine [0.4714mm 2.4912mm 0.5514mm 2.4912mm 6.00mil]
	ElementLine [0.5514mm 2.4912mm 1.4766mm 1.5660mm 6.00mil]

	)
