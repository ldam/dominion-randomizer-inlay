
Element["" "" "" "" 5.7571mm 3.2571mm 0.0000 0.0000 0 100 ""]
(
	Pad[-5.2500mm -2.2500mm -4.7500mm -2.2500mm 1.5000mm 20.00mil 1.7540mm "1" "1" "square"]
	Pad[-5.2500mm 2.2500mm -4.7500mm 2.2500mm 1.5000mm 20.00mil 1.7540mm "2" "2" "square"]
	Pad[4.2500mm -2.2500mm 4.7500mm -2.2500mm 1.5000mm 20.00mil 1.7540mm "1" "1" "square,edge2"]
	Pad[4.2500mm 2.2500mm 4.7500mm 2.2500mm 1.5000mm 20.00mil 1.7540mm "2" "2" "square,edge2"]
	ElementLine [-3.0000mm -2.5000mm 2.5000mm -2.5000mm 10.00mil]
	ElementLine [-3.0000mm 2.5000mm 2.5000mm 2.5000mm 10.00mil]
	ElementLine [-4.5000mm -1.0000mm -4.5000mm 1.0000mm 10.00mil]
	ElementLine [4.0000mm -1.0000mm 4.0000mm 1.0000mm 10.00mil]

	)
