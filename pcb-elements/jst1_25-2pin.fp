
Element["" "" "" "" 4.4675mm 4.1579mm -0.0350mm -0.0157mm 0 100 ""]
(
	Pad[-3.3500mm -0.6000mm -2.6000mm -0.6000mm 0.7000mm 20.00mil 0.9540mm "2" "2" "square"]
	Pad[-3.3500mm 0.6000mm -2.6000mm 0.6000mm 0.7000mm 20.00mil 0.9540mm "1" "1" "square"]
	Pad[-0.2500mm -2.9000mm 0.2500mm -2.9000mm 2.0000mm 20.00mil 2.2540mm "" "4" "square,edge2"]
	Pad[-0.2500mm 2.9000mm 0.2500mm 2.9000mm 2.0000mm 20.00mil 2.2540mm "" "3" "square,edge2"]
	ElementLine [-3.5500mm -3.7500mm -3.5500mm -1.7500mm 6.00mil]
	ElementLine [-3.5500mm 1.7500mm -3.5500mm 3.7500mm 6.00mil]
	ElementLine [-3.5500mm -3.7500mm -2.0500mm -3.7500mm 6.00mil]
	ElementLine [-3.5500mm 3.7500mm -2.0500mm 3.7500mm 6.00mil]
	ElementLine [2.5000mm -3.7500mm 2.5000mm 3.7500mm 6.00mil]
	ElementLine [2.5000mm -3.7500mm 2.0000mm -3.7500mm 6.00mil]
	ElementLine [2.5000mm 3.7500mm 2.0000mm 3.7500mm 6.00mil]
	ElementLine [-4.1500mm 1.9500mm -4.1500mm 1.9500mm 15.00mil]

	)
