#ifndef SW_WS2811_H
#define SW_WS2811_H

#define MODE_OFF 1
#define MODE_ON 2
#define MODE_GAME 3
#define MODE_ALL_GAME 4
#define MODE_RANDOMIZING 5
#define MODE_CONFIG 6

#define LED_COUNT 35

#define COLOR_OFF 0
#define COLOR_GREEN 1
#define COLOR_YELLOW 2
#define COLOR_BLUE 3
#define COLOR_RED 4
#define COLOR_WHITE 5

#define SLOTGROUP_NONE 0
#define SLOTGROUP_KINGDOM 1
#define SLOTGROUP_3PLAYER 2
#define SLOTGROUP_24PLAYER 3

typedef unsigned char SlotGroup;
typedef struct {
    SlotGroup slotType : 2;
    SlotGroup currentGame : 2;
    SlotGroup previousGame : 2;
} Slot;

void startNewSession();

void on();
void off();
void showGame();
void showAllGame();
void showRandomizing();
void showConfig();
unsigned char isModeOn();
unsigned char isModeOff();
unsigned char isModeGame();
unsigned char isModeAllGame();
unsigned char isModeRandomizing();
unsigned char isModeConfig();

void loadConfig();
void saveConfig();

void setConfigEditPos(unsigned char editPos);
void nextConfigEditPos();
void toggleTypeAtEditPos();

void saveOldGame();
void randomize();
unsigned char nextRandom();

void toggleFlash();
void sendLedData();
#endif //SW_WS2811_H
