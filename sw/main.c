#include "pic18f14k22.h"
#include "ws2811.h"

/*
MCU config
*/
#pragma config XINST = OFF
#pragma config PLLEN = ON
#pragma config IESO = ON
#pragma config PWRTEN = OFF
#pragma config MCLRE = OFF
#pragma config FOSC  = IRC
#pragma config WDTEN = OFF
#pragma config LVP   = OFF
#pragma config WDTPS = 8
#pragma config PCLKEN = ON

/*
 * HW mappings
 */
#define BUTTON_MASK_B_GREEN _RB4
#define BUTTON_MASK_B_RED _RB6
#define BUTTON_MASK_PORTA (0)
#define BUTTON_MASK_PORTB (BUTTON_MASK_B_GREEN|BUTTON_MASK_B_RED)

#define PORTA_INDEX 0
#define PORTB_INDEX 1
#define BUTTON_GREEN_PORT_INDEX PORTB_INDEX
#define BUTTON_RED_PORT_INDEX PORTB_INDEX


/*
 * hw configurations
 */
void initMCU() {
    // cpu speed
    OSCCONbits.SCS = 0b10;
    OSCCONbits.OSTS= 0;
    OSCCONbits.IRCF = 0b111;

    OSCTUNEbits.PLLEN = 1;
    OSCTUNEbits.INTSRC = 1;
    

    // go all digital input
    ANSEL = 0;
    ANSELH = 0;
    // outputs
    PORTA = 0x00;
    PORTB = 0x00;
    PORTC = 0x00;
    TRISC = 0x00;
    // inputs
    INTCON2bits.NOT_RABPU = 0; //pullups enabled
    TRISA = BUTTON_MASK_PORTA;
    WPUA = BUTTON_MASK_PORTA;
    TRISB = BUTTON_MASK_PORTB;
    WPUB = BUTTON_MASK_PORTB;
    // input irq
    IOCA = BUTTON_MASK_PORTA;
    IOCB = BUTTON_MASK_PORTB;
    INTCONbits.RABIF = 0;
    INTCONbits.RABIE = 1;


}

/*
 * Timing handling
 */
#define TIMER_PLAY_OFF 1650    // time to off when in play mode, ca 1 min
#define TIMER_CONFIG_OFF 1650  // time to iff when in config mode, ca 1 min
#define TIMER_NEXT_RANDOM 1    // time to next randomizing when starting a new game
unsigned int timerCount;

#define TIMER_MODE_NEXT_EVENT_OFF 0       // next timing event: turn off device
#define TIMER_MODE_NEXT_EVENT_RANDOM 1    // next timing event: randomize again, and count down randomize tries
unsigned char timerMode;

/*
 * Randomizing handling
 * Instead of randomizing once we do it a number of times to 'roll the dices' - as a visual effect
 */
#define RANDOMIZE_TRIES 29
unsigned char randomizeCount;

/*
 * Flash timing
 * Always active when device is not off. Used to flash selection when config mode
 */
#define FLASH_DELAY 5
unsigned char flashCount = FLASH_DELAY;

/*
 * Events
 */
#define EVENT_NONE_ID 0
#define EVENT_GREEN_DOWN 1
#define EVENT_GREEN_LONG_DOWN 2
#define EVENT_GREEN_UP 3
#define EVENT_RED_DOWN 4
#define EVENT_RED_LONG_DOWN 5
#define EVENT_RED_UP 6
#define EVENT_TIMER 7
#define EVENT_FLASH 8

/*
 * Execute events
 */
unsigned char seeding = 0;

void events(unsigned char event_id) {
    switch (event_id) {
        case EVENT_GREEN_DOWN:
            seeding = 1;
            break;
        case EVENT_GREEN_LONG_DOWN:
            if (isModeOn()) {
                setConfigEditPos(28);
                showConfig();
                timerMode = TIMER_MODE_NEXT_EVENT_OFF;
                timerCount = TIMER_CONFIG_OFF;
            } else if (isModeConfig()) {
                loadConfig(); // cancel (reload saved config)
                off();
            }
            break;
        case EVENT_GREEN_UP:
            seeding = 0;
            if (isModeOn()) {
                showAllGame();
                timerMode = TIMER_MODE_NEXT_EVENT_OFF;
                timerCount = TIMER_PLAY_OFF;
            } else if (isModeGame() || isModeAllGame()) {
                off();
            } else if (isModeConfig()) {
                nextConfigEditPos();
                timerCount = TIMER_CONFIG_OFF;
            }
            break;
        case EVENT_RED_DOWN:
            seeding = 1;
            break;
        case EVENT_RED_LONG_DOWN:
            if (isModeOn()) {
                startNewSession();
                randomize();
                showRandomizing();
                timerMode = TIMER_MODE_NEXT_EVENT_RANDOM;
                timerCount = TIMER_NEXT_RANDOM;
                randomizeCount = RANDOMIZE_TRIES;
            } else if (isModeConfig()) {
                saveConfig();
                off();
            } else if (isModeGame() || isModeAllGame()) {
                off();
            }
            break;
        case EVENT_RED_UP:
            seeding = 0;
            if (isModeOn()) {
                saveOldGame();
                randomize();
                showRandomizing();
                timerMode = TIMER_MODE_NEXT_EVENT_RANDOM;
                timerCount = TIMER_NEXT_RANDOM;
                randomizeCount = RANDOMIZE_TRIES;
            } else if (isModeGame() || isModeAllGame()) {
                off();
            } else if (isModeConfig()) {
                toggleTypeAtEditPos();
                timerCount = TIMER_CONFIG_OFF;
            }
            break;
        case EVENT_TIMER:
            if (timerMode == TIMER_MODE_NEXT_EVENT_OFF) {
                off();
            } else if (timerMode == TIMER_MODE_NEXT_EVENT_RANDOM) {
                randomizeCount--;
                if (randomizeCount) {
                    randomize();
                    timerCount = TIMER_NEXT_RANDOM;
                } else {
                    showGame();
                    timerMode = TIMER_MODE_NEXT_EVENT_OFF;
                    timerCount = TIMER_PLAY_OFF;
                }
            }
            break;
        case EVENT_FLASH:
            toggleFlash();
            break;
        default: {
        }
    }
}

/*
 * Event table
 */
//@formatter:off           input:          green button           red button
static const unsigned char press[]      = {EVENT_GREEN_DOWN,      EVENT_RED_DOWN};
static const unsigned char long_press[] = {EVENT_GREEN_LONG_DOWN, EVENT_RED_LONG_DOWN};
static const unsigned char release[]    = {EVENT_GREEN_UP,        EVENT_RED_UP};
//@formatter:on

/*
 * Button reading stuff
 */
#define BUTTON_DEBOUNCE_DELAY 2
#define BUTTON_LONG_PRESS_DELAY 25
static const unsigned char port_buttons[] = {BUTTON_MASK_B_GREEN, BUTTON_MASK_B_RED};
static const unsigned char button_port_index[] = {BUTTON_GREEN_PORT_INDEX, BUTTON_RED_PORT_INDEX};
unsigned char last_port[2];  // uses button_port_index
unsigned char button_debounce_time[] = {0, 0};    // count down to zero and execute press event when zero is reached. Pr. button
unsigned char button_long_press_time[] = {0, 0};  // count down to zero and execute long press event when zero is reached. Pr. button
unsigned char button_ignore_next_release[] = {0, 0}; // when a long press was done, ignore the actual button release

/*
 * Irq stuff
 */
//@formatter:off
static void isr(void) __interrupt 1 {
    unsigned char i;
    unsigned char port[2];         // uses button_port_index
    unsigned char change_port[2];  // uses button_port_index
    // handle port a+b input irq
    if (INTCONbits.RABIF){
        port[PORTA_INDEX] = PORTA;
        port[PORTB_INDEX] = PORTB;
        change_port[PORTA_INDEX] = (last_port[PORTA_INDEX]^port[PORTA_INDEX]);
        change_port[PORTB_INDEX] = (last_port[PORTB_INDEX]^port[PORTB_INDEX]);
        last_port[PORTA_INDEX] = port[PORTA_INDEX];
        last_port[PORTB_INDEX] = port[PORTB_INDEX];
        for (i = 0; i<sizeof(port_buttons);i++){
            if (change_port[button_port_index[i]] & port_buttons[i]){
                button_debounce_time[i] = BUTTON_DEBOUNCE_DELAY;
                if(isModeOff()){
                    on(); // starts wdt from off mode; makes it possible to use wdt for timing debounce delay before actual press actions are done
                }
            }
        }
        INTCONbits.RABIF = 0;
    }
}
//@formatter:on

/*
 * Main
 */
int main() {
    unsigned char i;
    initMCU();
    off();
    last_port[PORTA_INDEX] = PORTA;
    last_port[PORTB_INDEX] = PORTB;
    loadConfig();
    startNewSession();
    randomize();
    // now enable irqs and start running
    INTCONbits.GIE = 1;
    while (1) {
        sendLedData();

        // enable WDT if we're not off - means we're waiting for timing events or long press events, etc.
        // if we're off, then we only wait for keypresses and not timing events. Key presses they trigger the irq and resume after sleep
        WDTCONbits.SWDTEN = isModeOff() ? 0 : 1;
        __asm sleep __endasm;
        WDTCONbits.SWDTEN = 0;

        // if wdt event, check timings
        if (!RCONbits.NOT_TO) {
            if (seeding) {
                nextRandom();
            }
            // check button timings
            for (i = 0; i < sizeof(port_buttons); i++) {
                // check debounce
                if (button_debounce_time[i]) {
                    button_debounce_time[i]--;
                    if (!button_debounce_time[i]) {
                        if (last_port[button_port_index[i]] & port_buttons[i]) {
                            if (button_ignore_next_release[i]) {
                                button_ignore_next_release[i] = 0;
                            } else {
                                events(release[i]);
                            }
                            button_long_press_time[i] = 0;
                        } else {
                            events(press[i]);
                            button_long_press_time[i] = BUTTON_LONG_PRESS_DELAY;
                        }
                    }
                }
                // check long presses
                if (button_long_press_time[i]) {
                    button_long_press_time[i]--;
                    if (!button_long_press_time[i]) {
                        events(long_press[i]);
                        button_ignore_next_release[i] = 1;
                    }
                }
            }
            // repeat event
            if (timerCount) {
                timerCount--;
                if (!timerCount) {
                    events(EVENT_TIMER);
                }
            }
            // count flash delay; flash events are always enabled
            flashCount--;
            if (!flashCount) {
                flashCount = FLASH_DELAY;
                events(EVENT_FLASH);
            }
        }
    }
}
