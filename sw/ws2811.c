#include "pic18f14k22.h"
#include "ws2811.h"

#define LED_POWER PORTBbits.RB7
#define PIN_WS2811_SIGNAL TRISBbits.TRISB5


/*
 * Initial eeprom configuration
 */
#define EEPROM_SLOT_CONFIG 0
//@formatter:off
__code SlotGroup __at 0xf00000 __EEPROM[] = {
    SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_NONE,
    SLOTGROUP_KINGDOM,SLOTGROUP_NONE,SLOTGROUP_24PLAYER,SLOTGROUP_KINGDOM,SLOTGROUP_24PLAYER,SLOTGROUP_24PLAYER,SLOTGROUP_3PLAYER,
    SLOTGROUP_3PLAYER,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,
    SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_3PLAYER,
    SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,
    SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_KINGDOM,SLOTGROUP_24PLAYER
};
//@formatter:on

/*
 * Slots - keeps track of purpose, current game, previous game for each led
 */
Slot slots[LED_COUNT];

#define BRIGHTNESS_LEVEL 0x20
/*
 * colors
 */
const unsigned char color_data[] = {
        // G    R    B
        0x00, 0x00, 0x00,
        BRIGHTNESS_LEVEL, 0x00, 0x00,
        BRIGHTNESS_LEVEL >> 1, BRIGHTNESS_LEVEL, 0x00,
        0x00, 0x00, BRIGHTNESS_LEVEL,
        0x00, BRIGHTNESS_LEVEL, 0x00,
        BRIGHTNESS_LEVEL, BRIGHTNESS_LEVEL, BRIGHTNESS_LEVEL
};

/*
 * The mode
 */
unsigned char mode = MODE_OFF;

/*
 * Randomize
 */
unsigned int seed;

unsigned char nextRandom() {
    seed = seed * 221 + 53;
    return seed;
}

#define WE_ARE_THERE 0
unsigned char slotPointer = 0;

int getRandomFreeSlotOfType(SlotGroup slotType) {
    unsigned char notFoundCount = LED_COUNT; // if this reaches zero, then no more slots are available
    unsigned char areWeThereYet = nextRandom() % LED_COUNT;
    // circulate through leds, count r down everytime a free slotType is found. When r reaches 0, return this slot
    while (1) {
        if (slots[slotPointer].slotType == slotType && slots[slotPointer].currentGame == SLOTGROUP_NONE) {
            notFoundCount = LED_COUNT; // reset no-slot-found detection
            areWeThereYet--;
            if (areWeThereYet == WE_ARE_THERE) {
                return slotPointer;
            }
        }
        notFoundCount--;
        if (!notFoundCount) {
            return -1;
        }
        slotPointer++;
        if (slotPointer >= LED_COUNT) {
            slotPointer = 0;
        }
    }
}

void randomize() {
    unsigned char i;
    int slotPos;
    // clear current game
    for (i = 0; i < LED_COUNT; i++) {
        slots[i].currentGame = SLOTGROUP_NONE;
    }
    // find one player 3 slot
    slotPos = getRandomFreeSlotOfType(SLOTGROUP_3PLAYER);
    if (slotPos >= 0) {
        slots[slotPos].currentGame = SLOTGROUP_3PLAYER;
    }
    // find one player 2+4 slot
    slotPos = getRandomFreeSlotOfType(SLOTGROUP_24PLAYER);
    if (slotPos >= 0) {
        slots[slotPos].currentGame = SLOTGROUP_24PLAYER;
    }
    // find ten kingdom slots
    for (i = 0; i < 10; i++) {
        slotPos = getRandomFreeSlotOfType(SLOTGROUP_KINGDOM);
        if (slotPos >= 0) {
            slots[slotPos].currentGame = SLOTGROUP_KINGDOM;
        }
    }
}

void startNewSession() {
    // clear old game data
    unsigned char i;
    for (i = 0; i < LED_COUNT; i++) {
        slots[i].previousGame = SLOTGROUP_NONE;
    }
}

/*
 * Eeprom data
 */
SlotGroup buffer;

void loadConfig() {
    unsigned char i;
    for (i = 0; i < LED_COUNT; i++) {
        EEADR = EEPROM_SLOT_CONFIG + i;
        // load types from eeprom
//@formatter:off
        __asm
        BANKSEL _EECON1
        BCF     _EECON1,7    ;EEPGD
        BCF     _EECON1,6    ;CFGS
        BSF     _EECON1,0    ;RD
        BANKSEL _EEDATA
        MOVF    _EEDATA,0
        BANKSEL _buffer
        movwf   _buffer
        __endasm;
//@formatter:on
        slots[i].slotType = buffer;
    }
}

void saveConfig() {
    unsigned char i;
    for (i = 0; i < LED_COUNT; i++) {
        EEADR = EEPROM_SLOT_CONFIG + i;
        buffer = slots[i].slotType;

//@formatter:off
        __asm
        BANKSEL _buffer
        MOVF    _buffer,0
        MOVWF   _EEDATA          ; Data Memory Value to write
        BCF     _EECON1, 7       ; EEPGDi+10
        BCF     _EECON1, 6       ; Access EEPROM
        BSF     _EECON1, 2       ; WREN Enable writes
        BCF     _INTCON, 7       ; GIE Disable Interrupts
        MOVLW   55h
        MOVWF   _EECON2          ; Write 55h
        MOVLW   0AAh
        MOVWF   _EECON2          ; Write 0AAh
        BSF     _EECON1, 1       ; Set WR bit to begin write
        BSF     _INTCON, 7       ; GIE Enable Interrupts
        BCF     _EECON1, 2       ; WREN Disable writes on write complete (EEIF set)
        __endasm;
//@formatter:on
        while (EECON1bits.WR);    // wait until stuff is saved in eeprom
    }
}

/*
 * Config interactions
 */
unsigned char configEditPos;

void setConfigEditPos(unsigned char editPos) {
    configEditPos = editPos;
}

void nextConfigEditPos() {
    configEditPos = configEditPos + 1;
    if (configEditPos >= LED_COUNT) {
        configEditPos = 0;
    }
}

void toggleTypeAtEditPos() {
    slots[configEditPos].slotType =
            slots[configEditPos].slotType == SLOTGROUP_NONE ? SLOTGROUP_KINGDOM :
            slots[configEditPos].slotType == SLOTGROUP_KINGDOM ? SLOTGROUP_3PLAYER :
            slots[configEditPos].slotType == SLOTGROUP_3PLAYER ? SLOTGROUP_24PLAYER :
            SLOTGROUP_NONE;
}

/*
 * Main interactions
 */
void on() {
    mode = MODE_ON;
    PIN_WS2811_SIGNAL = 0;
    LED_POWER = 1;
}

void off() {
    mode = MODE_OFF;
    sendLedData();
    LED_POWER = 0;
    PIN_WS2811_SIGNAL = 1;
}

void showGame() {
    mode = MODE_GAME;
    LED_POWER = 1;
}

void showAllGame() {
    mode = MODE_ALL_GAME;
    LED_POWER = 1;
}

void showRandomizing() {
    mode = MODE_RANDOMIZING;
    LED_POWER = 1;
}

void showConfig() {
    mode = MODE_CONFIG;
    LED_POWER = 1;
}

unsigned char isModeOn() {
    return mode == MODE_ON ? 1 : 0;
}

unsigned char isModeOff() {
    return mode == MODE_OFF ? 1 : 0;
}

unsigned char isModeGame() {
    return mode == MODE_GAME ? 1 : 0;
}

unsigned char isModeAllGame() {
    return mode == MODE_ALL_GAME ? 1 : 0;
}

unsigned char isModeRandomizing() {
    return mode == MODE_RANDOMIZING ? 1 : 0;
}

unsigned char isModeConfig() {
    return mode == MODE_CONFIG ? 1 : 0;
}

void saveOldGame() {
    unsigned char i;
    for (i = 0; i < LED_COUNT; i++) {
        slots[i].previousGame = slots[i].currentGame;
    }
}

/*
 * flash update
 */
unsigned char flash = 0;

void toggleFlash() {
    flash = 1 - flash;
}

/*
 * Led update
 */
unsigned char ledColor[LED_COUNT];

void sendLedData() {
    unsigned char l;
    unsigned char led_pos;
    unsigned char colorIndex;

    for (led_pos = 0; led_pos < LED_COUNT; led_pos++) {
        switch (mode) {
            case MODE_RANDOMIZING:
            case MODE_GAME:
            case MODE_ALL_GAME:
                if (slots[led_pos].previousGame == SLOTGROUP_KINGDOM && slots[led_pos].currentGame == SLOTGROUP_NONE) {
                    ledColor[led_pos] = COLOR_RED;
                } else if (slots[led_pos].previousGame == SLOTGROUP_KINGDOM && slots[led_pos].currentGame == SLOTGROUP_KINGDOM && mode == MODE_GAME) {
                    ledColor[led_pos] = COLOR_OFF;
                } else {
                    ledColor[led_pos] = slots[led_pos].currentGame;
                }
                break;
            case MODE_CONFIG:
                if (led_pos == configEditPos && flash) {
                    ledColor[led_pos] = COLOR_OFF;
                } else if (slots[led_pos].slotType == COLOR_OFF) {
                    ledColor[led_pos] = COLOR_WHITE;
                } else {
                    ledColor[led_pos] = slots[led_pos].slotType;
                }
                break;
            default:
                ledColor[led_pos] = COLOR_OFF;
                break;
        }
    }

    // Start ws2811b timing sensitive section
    INTCONbits.GIE = 0; // no irq during the timing sensitive led data transmission
    for (led_pos = 0; led_pos < LED_COUNT; led_pos++) {
        colorIndex = ledColor[led_pos] * 3;
        for (l = 0; l < 3; l++) {    // send RGB (we're only doing white color always)
            TMR3H = 0x08;            // send 8 bits (abusing tmr3 regs to ensure correct bank)
            TMR3L = color_data[colorIndex++];
            //@formatter:off
            __asm
            // T0L & T1L is a bit longer (~1-2.5uS) than spec of 1uS, however far less than reset of 250uS, and seems to work with ws2812b
_ws2811_next:
            rlcf    _TMR3L,1     ; roll MSB into carry
            btfss   _STATUS, 0   ; jump to zero or one timing
            goto    _zero

_one:       // gives about 800ns spike, within spec for T1H
            bsf     _PORTB, 5
            nop
            nop
            bcf     _PORTB, 5
            goto _ws2811_cont

_zero:      // gives about 250ns spike, within spec for T0H
            bsf     _PORTB, 5
            bcf     _PORTB, 5

_ws2811_cont:
            decfsz  _TMR3H, 1    ; test for more bits
            goto    _ws2811_next ; more bits, go do the next

            rlcf    _TMR3L,1     ;  roll carry back into reg
            __endasm;
            //@formatter:on
        }
    }
    INTCONbits.GIE = 1;   // re-enable irq
    for (l = 0; l < 50; l++);      // do a 50uS (measured to 54.12uS) delay here for the ws2811 protocol reset
    // End ws2811b timing sensitive section
}
